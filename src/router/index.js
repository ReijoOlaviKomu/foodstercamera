import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/HomeView'
import Camera from '@/components/CameraView'
import Login from '@/components/User/LoginView'
import SignUp from '@/components/User/SignUpView'
import firebase from 'firebase'
Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta:{
      	requiresAuth:true
      }
    },
    {
    	path:'/login',
    	name:'login',
    	component:Login,
		meta:{
			requiresGuest:true
		}
    },
    {
    	path:'/signup',
    	name:'signup',
    	component:SignUp,
		meta:{
			requiresGuest:true
		}    	
    },
    {
    	path:'/camera',
    	name:'camera',
    	component:Camera,
		meta:{
			requiresAuth:true
		}
    }

  ]
})
//Nav Guards
router.beforeEach((to,from,next) => {
	//Check for requreAuth Guard
	if(to.matched.some(record => record.meta.requiresAuth)){
		// Check if Not logged In
		if(!firebase.auth().currentUser){
			next({
					path: '/login',
					query:{
						redirect:to.fullPath
					}
				});
		}else{
			next();
		}
	}else if(to.matched.some(record => record.meta.requiresGuest)){

		if(firebase.auth().currentUser){
			next({
					path: '/',
					query:{
						redirect:to.fullPath
					}
				});
		}else{
			next();
		}

	} else{
		next()
	}
});

export default router;

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ToggleButton from 'vue-js-toggle-button'
import firebase from 'firebase'

Vue.use(ToggleButton)
Vue.config.productionTip = false

var config = {
  apiKey: "AIzaSyBrhGnEJS8zNP6FVQdPBwRtlz4TxIoUXTk",
  authDomain: "foodstercamera.firebaseapp.com",
  databaseURL: "https://foodstercamera.firebaseio.com",
  projectId: "foodstercamera",
  storageBucket: "foodstercamera.appspot.com",
  messagingSenderId: "43914181063"
};
firebase.initializeApp(config);


let app;
firebase.auth().onAuthStateChanged(user => {
	if(!app){
		app = new Vue({
			el: '#app',
			router,
			components: { App },
			template: '<App/>'
		})
	}
});


